
let new_game = document.getElementById('new_game');
let option = document.getElementById('option');
let help = document.getElementById('help');
let team = document.getElementById('team');

new_game.addEventListener('click', function (){
    document.location.href = "/introduction";
});

option.addEventListener('click', function (){
    document.location.href = "option";
});

help.addEventListener('click', function (){
    document.location.href = "help";
});

team.addEventListener('click', function (){
    document.location.href = "team";
});