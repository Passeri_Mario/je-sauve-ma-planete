    // Detection des touches et souris en jeu

    document.oncontextmenu = new Function("return false");

    window.addEventListener('keydown', function(event) {
        if(event.code === "F1"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "F2"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F3"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F4"){
             event.preventDefault();
            return false;
        }
        else if (event.code === "F5") {
            if (confirm("Attention, si vous confirmer la demande, vous perdrez votre progression du jeu, contacter le professeur avant.")) {
                document.location.href="/";
            }
        }
        else if(event.code === "F6"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F7"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F8"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "F9"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F10"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "F12"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "AltLeft"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "Digit4"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "ContextMenu"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "ControlLeft"){
             event.preventDefault();
            return false;
        }
        else if(event.code === "ShiftLeft"){
            event.preventDefault();
            return false;
        }
        else if(event.code === "NumpadDivide"){
            event.preventDefault();
            return false;
        }
    });