
let verif = document.getElementById("Verif");
verif.addEventListener("click", function (){

    erreurs=0;

    let rep1 = document.getElementById("rep1");
    let rep2 = document.getElementById("rep2");
    let rep3 = document.getElementById("rep3");
    let rep4 = document.getElementById("rep4");
    let rep5 = document.getElementById("rep5");
    let rep6 = document.getElementById("rep6");

    if(rep1.value!==""
        && rep2.value!==""
        && rep3.value!==""
        && rep4.value!==""
        && rep5.value!==""
        && rep5.value!==""
        && rep6.value!==""){

        if (document.form2.rep1.value!=="rectangle"){
            document.form2.rep1.style.color="red";
            document.form2.rep1.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep1.style.color="green";
            document.form2.rep1.readOnly = true;

        }

        if (document.form2.rep2.value!=="180"){
            document.form2.rep2.style.color="red";
            document.form2.rep2.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep2.style.color="green";
            document.form2.rep2.readOnly = true;
        }

        if (document.form2.rep3.value!=="181"){
            document.form2.rep3.style.color="red";
            document.form2.rep3.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep3.style.color="green";
            document.form2.rep3.readOnly = true;
        }

        if (document.form2.rep4.value!=="182"){
            document.form2.rep4.style.color="red";
            document.form2.rep4.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep4.style.color="green";
            document.form2.rep4.readOnly = true;
        }

        if (document.form2.rep5.value!=="183"){
            document.form2.rep5.style.color="red";
            document.form2.rep5.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep5.style.color="green";
            document.form2.rep5.readOnly = true;
        }

        if (document.form2.rep6.value!=="184"){
            document.form2.rep6.style.color="red";
            document.form2.rep6.readOnly = true;
            erreurs++;
        }
        else {
            document.form2.rep6.style.color="green";
            document.form2.rep6.readOnly = true;
        }

        function message(erreurs) {

            let date = new Date(Date.now() + 86400000); //86400000ms = 1 jour
            date = date.toUTCString()

            if (erreurs === 0){
                document.cookie = 'examen='+erreurs+'; path=/; expires=' + date;
                document.location.href="/end";
            }
            else if (erreurs === 1){
                document.cookie = 'examen='+erreurs+'; path=/; expires=' + date;
                document.location.href="/end";
            }
            else{
                document.cookie = 'examen='+erreurs+'; path=/; expires=' + date;
                document.location.href="/end";
            }
        }
        message(erreurs);
    }
})



