<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Voici les routes pour enregistrer des itinéraires Web du jeu vidéo
    | Les routes sont chargées par $_SERVER['REQUEST_URI']
    |

    */

    // Configuration de l'application

    include ('config/conf.php');

    $request = $_SERVER['REQUEST_URI'];

    switch ($request) {

        // 404
        default:
            http_response_code(404);
            require __DIR__ . '/controller/Error/404.php';
            break;

        //  Configuration du jeu

        case '/' :
        case '/index':
            http_response_code(200);
            require __DIR__ . '/controller/Menu/start_game.php';
            break;

        case '/introduction':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/introduction.php';
            break;

        case '/introduction_config':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/introduction_config.php';
            break;

        case '/tutorial':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/tutorial.php';
            break;

        // Lancement du jeu

        case '/world_1?q=question1':
        case '/world_1?q=question2':
        case '/world_1?q=question3':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/world_1.php';
            break;

        // Pages erreurs
        case '/world_error_1?er=question1':
        case '/world_error_1?er=question2':
        case '/world_error_1?er=question3':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/world_error_1.php';
            break;

        case '/world_2?q=question1':
        case '/world_2?q=question2':
        case '/world_2?q=question3':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/world_2.php';
            break;

        // pages erreurs
        case '/world_error_2?er=question1':
        case '/world_error_2?er=question2':
        case '/world_error_2?er=question3':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/world_error_2.php';
            break;

        // Résultats du jeu
        case '/end_game':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/end_game.php';
            break;

        // texte à trous
        case '/examen':
            http_response_code(200);
            require __DIR__ . '/controller/Examen/examen.php';
            break;

        // fin complet de l'application
        case '/end':
            http_response_code(200);
            require __DIR__ . '/controller/Mondes/end.php';
            break;


    }