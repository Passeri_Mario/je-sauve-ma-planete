<?php

    include ("config/conf.php");

    session_destroy();

    // Suppression des Cookies
    foreach($_COOKIE as $cookie_name => $cookie_value){
        unset($_COOKIE[$cookie_name]);
        setcookie($cookie_name, '', time() - 4200, '/');
    }

    include("view/start_game.phtml");