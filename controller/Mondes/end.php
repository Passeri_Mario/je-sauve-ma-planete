<?php

    session_start();

    htmlspecialchars($examen = $_COOKIE['examen']);

    if(isset($examen)){
        htmlspecialchars($groupe = $_COOKIE['groupe']);
        htmlspecialchars($prenom1 = $_COOKIE['prenom1']);
        htmlspecialchars($prenom2 = $_COOKIE['prenom2']);
        htmlspecialchars($prenom3 = $_COOKIE['prenom3']);
        htmlspecialchars($prenom4 = $_COOKIE['prenom4']);

        htmlspecialchars($bonne_reponse = $_COOKIE['bonne_reponse']);
        htmlspecialchars($mauvaise_reponse = $_COOKIE['mauvaise_reponse']);

        include("view/end.phtml");
    }
    else{
        header('Location: /');
    }
    