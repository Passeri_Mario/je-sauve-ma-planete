<?php

    session_start();

    // Gestion CRSF
    htmlspecialchars($select = $_POST['select']);
    $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
    $_SESSION['token'] = $token;

    htmlspecialchars($erreur = $_GET['er']);

    $groupe = $_COOKIE['groupe'];
    $prenom1 = $_COOKIE['prenom1'];
    $prenom2 = $_COOKIE['prenom2'];
    $prenom3 = $_COOKIE['prenom3'];
    $prenom4 = $_COOKIE['prenom4'];

    $bonne_reponse = $_COOKIE['bonne_reponse'];
    $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];

    // Configuration des du monde -----------------------------------------------------------------------------------------------------------

    $monde2Titre->Nom_du_monde = "ERREUR !! ";

    $monde2Question1->texte = 'La bonne réponse est dans les industries
                              <br><br>
                               Un responsable environnement travaille dans dans les industries pour réduire la quantité de ressources nécessaires pour créer des
                               emballages et utiliser le plus possible de matériau recyclable.
                               <br><br>
                               Il a pour but de réduire l’impact environnemental des produits pour polluer moins.';

    $monde2Question2->texte = 'La bonne réponse est NON 
                               <br><br>
                               Des microparticules de plastique sont plus facilement détachables, il existe un risque de les ingérer. 
                               <br><br>
                               Le plastique n’est pas bon pour la santé lorsqu’il est ingéré.';

    $monde2Question3->texte = 'La bonne réponse est NON
                               <br><br>
                               Seules les bouteilles de yaourt peuvent être recyclées.
                               <br><br>
                               Les pots de yaourt sont donc des emballages qui polluent la nature, car ils sont trop friables pour être traités.
                               <br><br>
                               En moyenne, un français jette 168 pots de yaourt par an.';


    $bouton1Question1_True->nom_bouton_1 = 'Recommencer';

    // END -----------------------------------------------------------------------------------------------------------------------------------

    include("view/world_error_2.phtml");
