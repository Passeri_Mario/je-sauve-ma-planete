<?php

    session_start();

    if (isset($_SESSION['token']) AND isset($_POST['token']) AND !empty($_SESSION['token']) AND !empty($_POST['token'])) {

        if ($_SESSION['token'] == $_POST['token']) {

            // Gestion CRSF
            htmlspecialchars($select = $_POST['select']);
            $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
            $_SESSION['token'] = $token;

            htmlspecialchars($groupe = $_POST['groupe']);
            htmlspecialchars($prenom1 = $_POST['prenom1']);
            htmlspecialchars($prenom2 = $_POST['prenom2']);
            htmlspecialchars($prenom3 = $_POST['prenom3']);
            htmlspecialchars($prenom4 = $_POST['prenom4']);

            include("view/tutorial.phtml");
        }
    }

