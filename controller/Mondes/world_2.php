<?php

    session_start();

        // Gestion CRSF
        $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
        $_SESSION['token'] = $token;

        // Variables d'URL
        htmlspecialchars($question = $_GET['q']);
        htmlspecialchars($erreur = $_GET['er']);

        htmlspecialchars($submitError1 = $_POST['submitError1']);
        htmlspecialchars($submitError2 = $_POST['submitError2']);
        htmlspecialchars($submitError3 = $_POST['submitError3']);

        htmlspecialchars($question1 = $_POST['question1']);
        htmlspecialchars($question2 = $_POST['question2']);
        htmlspecialchars($question3 = $_POST['question3']);

        // Variable d'affichage

        htmlspecialchars($groupe = $_COOKIE['groupe']);
        htmlspecialchars($prenom1 = $_COOKIE['prenom1']);
        htmlspecialchars($prenom2 = $_COOKIE['prenom2']);
        htmlspecialchars($prenom3 = $_COOKIE['prenom3']);
        htmlspecialchars($prenom4 = $_COOKIE['prenom4']);

        htmlspecialchars($bonne_reponse  = $_COOKIE['bonne_reponse']);
        htmlspecialchars($mauvaise_reponse = $_COOKIE['mauvaise_reponse']);

        // Traitement du scores pour les réponses

        if (isset($_POST['submitquestion1'])) {
            $bonne_reponse = $_COOKIE['bonne_reponse'];
            $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
            if ($question1 == "1") {
                $bonne_reponse = $bonne_reponse +1;
                setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
            } else {
                $mauvaise_reponse = $mauvaise_reponse +1;
                setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                header('Location: /world_error_2?er=question1');
                exit();
            }
        }

        if (isset($_POST['submitquestion2'])) {
            $bonne_reponse = $_COOKIE['bonne_reponse'];
            $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
            if ($question2 == "1") {
                $bonne_reponse = $bonne_reponse +1;
                setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
            } else {
                $mauvaise_reponse = $mauvaise_reponse +1;
                setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                header('Location: /world_error_2?er=question2');
                exit();
            }
        }

        if (isset($_POST['submitquestion3'])) {
            $bonne_reponse = $_COOKIE['bonne_reponse'];
            $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
            if ($question3 == "1") {
                $bonne_reponse = $bonne_reponse +1;
                setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
                header('Location: /end_game');

            } else {
                $mauvaise_reponse = $mauvaise_reponse +1;
                setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                header('Location: /world_error_2?er=question3');
                exit();
            }
        }

        // Configuration des du monde -----------------------------------------------------------------------------------------------------------

        $monde2Video->video = 'assets/video/2.mp4';

        $monde2Question1->texte = 'Ou travaille un responsable environnement ?';

        $monde2Question2->texte = 'Le plastique peut-il être réutilisé pour faire des emballages alimentaires ?';

        $monde2Question3->texte = 'Les pots de yaourts peuvent-ils être transformés en granulés pour en faire des bottes ?';

        $bouton1Question1_True->nom_bouton_1 = 'Dans un centre de tri';
        $bouton2Question1_False->nom_bouton_2 = 'Dans les industries';

        $bouton1Question2_True->nom_bouton_1 = 'OUI';
        $bouton2Question2_False->nom_bouton_2 = 'NON';

        $bouton1Question3_True->nom_bouton_1 = 'OUI';
        $bouton2Question3_False->nom_bouton_2 = 'NON';


        // END -----------------------------------------------------------------------------------------------------------------------------------

        include("view/world_2.phtml");
