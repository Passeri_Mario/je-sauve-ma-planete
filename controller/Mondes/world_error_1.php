<?php

    session_start();

    // Gestion CRSF
    htmlspecialchars($select = $_POST['select']);
    $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
    $_SESSION['token'] = $token;

    htmlspecialchars($erreur = $_GET['er']);

    $groupe = $_COOKIE['groupe'];
    $prenom1 = $_COOKIE['prenom1'];
    $prenom2 = $_COOKIE['prenom2'];
    $prenom3 = $_COOKIE['prenom3'];
    $prenom4 = $_COOKIE['prenom4'];

    $bonne_reponse = $_COOKIE['bonne_reponse'];
    $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];

    // Configuration des du monde -----------------------------------------------------------------------------------------------------------

    $monde1Titre->Nom_du_monde = "ERREUR !! ";

    $monde1reponse1->texte = 'La bonne réponse est OUI 
                              <br><br> 
                              Il faut trier le verre.  
                              <br><br> 
                              La poubelle Jaune n’est pas pour le verre.
                              <br><br>
                              Il faut utiliser un point de collecte dans la rue qui est spécifique au verre.
                              <br><br>
                              Le verre est refondu pour faire d’autres produits en verre.';


    $monde1Reponse2->texte = 'La bonne réponse est OUI 
                              <br><br>
                              Les briques alimentaires sont faites de carton à 75% qui est recyclable.
                              <br><br>
                              Ainsi que du plastique et d’aluminium qui seront recyclés en mobilier urbain, mobilier d’intérieur et
                              tuiles.';

    $monde1Reponse3->texte = 'La bonne réponse est NON 
                              <br><br>
                              Il faut trier les déchets selon leur nature. 
                              <br><br>
                              Chaque matériau a son traitement spécifique
                              pour être réutilisé.                          
                              <br><br>Les journaux sont transformés en papier, les canettes 
                              sont réutilisées dans l\'industrie automobile et l’électroménager.';

    $bouton1Question1_True->nom_bouton_1 = 'Recommencer';

    // END -----------------------------------------------------------------------------------------------------------------------------------

    include("view/world_error_1.phtml");
