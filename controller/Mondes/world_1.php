<?php

    session_start();

    if (isset($_SESSION['token']) AND isset($_POST['token']) AND !empty($_SESSION['token']) AND !empty($_POST['token'])) {

        if ($_SESSION['token'] == $_POST['token']) {

            // Gestion CRSF
            $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
            $_SESSION['token'] = $token;

            // Variables d'URL
            htmlspecialchars($question = $_GET['q']);
            htmlspecialchars($erreur = $_GET['er']);

            htmlspecialchars($submitError1 = $_POST['submitError1']);
            htmlspecialchars($submitError2 = $_POST['submitError2']);
            htmlspecialchars($submitError3 = $_POST['submitError3']);

            htmlspecialchars($question1 = $_POST['question1']);
            htmlspecialchars($question2 = $_POST['question2']);
            htmlspecialchars($question3 = $_POST['question3']);

            // Variable d'affichage

            if (isset($submitError1) || isset($submitError2) || isset($submitError3)) {
                htmlspecialchars($groupe = $_COOKIE['groupe']);
                htmlspecialchars($prenom1 = $_COOKIE['prenom1']);
                htmlspecialchars($prenom2 = $_COOKIE['prenom2']);
                htmlspecialchars($prenom3 = $_COOKIE['prenom3']);
                htmlspecialchars($prenom4 = $_COOKIE['prenom4']);
                htmlspecialchars($bonne_reponse = $_COOKIE['bonne_reponse']);
                htmlspecialchars($mauvaise_reponse = $_COOKIE['mauvaise_reponse']);
            }
            else{
                htmlspecialchars($groupe = $_POST['groupe']);
                htmlspecialchars($prenom1 = $_POST['prenom1']);
                htmlspecialchars($prenom2 = $_POST['prenom2']);
                htmlspecialchars($prenom3 = $_POST['prenom3']);
                htmlspecialchars($prenom4 = $_POST['prenom4']);

                // Copies des données sur les cookies
                setcookie('groupe', $groupe, time()+3600*24, '/', '', true, true);
                setcookie('prenom1', $prenom1, time()+3600*24, '/', '', true, true);
                setcookie('prenom2', $prenom2, time()+3600*24, '/', '', true, true);
                setcookie('prenom3', $prenom3, time()+3600*24, '/', '', true, true);
                setcookie('prenom4', $prenom4, time()+3600*24, '/', '', true, true);
            }

            // Traitement du scores pour les réponses

            if (isset($_POST['submitquestion1'])) {
                $bonne_reponse = $_COOKIE['bonne_reponse'];
                $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
                if ($question1 == "1") {
                    $bonne_reponse = $bonne_reponse +1;
                    setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
                } else {
                    $mauvaise_reponse = $mauvaise_reponse +1;
                    setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                    header('Location: /world_error_1?er=question1');
                    exit();
                }
            }

            if (isset($_POST['submitquestion2'])) {
                $bonne_reponse = $_COOKIE['bonne_reponse'];
                $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
                if ($question2 == "1") {
                    $bonne_reponse = $bonne_reponse +1;
                    setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
                } else {
                    $mauvaise_reponse = $mauvaise_reponse +1;
                    setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                    header('Location: /world_error_1?er=question2');
                    exit();
                }
            }

            if (isset($_POST['submitquestion3'])) {
                $bonne_reponse = $_COOKIE['bonne_reponse'];
                $mauvaise_reponse = $_COOKIE['mauvaise_reponse'];
                if ($question3 == "1") {
                    $bonne_reponse = $bonne_reponse +1;
                    setcookie('bonne_reponse', $bonne_reponse, time()+3600*24, '/', '', true, true);
                    header('Location: /world_2?q=question1');

                } else {
                    $mauvaise_reponse = $mauvaise_reponse +1;
                    setcookie('mauvaise_reponse', $mauvaise_reponse, time()+3600*24, '/', '', true, true);
                    session_destroy();
                    header('Location: /world_error_1?er=question3');
                    exit();
                }
            }

            // Configuration des du monde -----------------------------------------------------------------------------------------------------------

            $monde1Video->video = 'assets/video/1.mp4';

            $monde1Question1->texte = 'Est-il utile de trier le verre ?';

            $monde1Question2->texte = 'Les briques alimentaires sont-elles recyclables ?';

            $monde1Question3->texte = 'Les emballages dans les poubelles jaunes sont-ils envoyés directement en centre de recyclage ?';

            $bouton1Question1_True->nom_bouton_1 = 'OUI';
            $bouton1Question1_False->nom_bouton_2 = 'NON';

            $bouton1Question2_True->nom_bouton_1 = 'OUI';
            $bouton1Question2_False->nom_bouton_2 = 'NON';

            $bouton1Question3_True->nom_bouton_1 = 'OUI';
            $bouton1Question3_False->nom_bouton_2 = 'NON';


            // END -----------------------------------------------------------------------------------------------------------------------------------

            include("view/world_1.phtml");
        }
    }