<?php

    session_start();

    if (isset($_SESSION['token']) AND isset($_POST['token']) AND !empty($_SESSION['token']) AND !empty($_POST['token'])) {

        if ($_SESSION['token'] == $_POST['token']) {

            htmlspecialchars($select = $_POST['select']);
            $token = md5(bin2hex(openssl_random_pseudo_bytes(6)));
            $_SESSION['token'] = $token;

            if($select == "fille"){
                htmlspecialchars($image = "fille.png");
                $form = '<form method="post" action="/tutorial">
                            <input type="text" id="prenom1" name="prenom1" placeholder="Ton Prénom" required="required" minlength="3" maxlength="10">       
                             <input type="hidden" name="token" id="token" value="'.$token.'"/>               
                            <input type="submit" value="Valider">
                        </form>';
            }

            elseif ($select == "garcon"){
                htmlspecialchars($image = "garçon.png");
                $form = '<form method="post" action="/tutorial">
                            <input type="text" id="prenom1" name="prenom1" placeholder="Ton Prénom" required="required" minlength="3" maxlength="10">    
                              <input type="hidden" name="token" id="token" value="'.$token.'"/>                            
                            <input type="submit" value="Valider">
                        </form>';
            }

            elseif ($select == "groupe"){
                htmlspecialchars($image = "groupe.jpg");
                $form = '<form method="post" action="/tutorial">
                            <input type="text" id="groupe" name="groupe" placeholder="Nom du groupe">                           
                            <input type="text" id="prenom1" name="prenom1" placeholder="Prénom 1" required="required" minlength="3" maxlength="10"> 
                            <input type="text" id="prenom2" name="prenom2" placeholder="Prénom 2" required="required" minlength="3" maxlength="10">      
                            <input type="text" id="prenom3" name="prenom3" placeholder="Prénom 3 facultatif" minlength="3" maxlength="10">      
                            <input type="text" id="prenom4" name="prenom4" placeholder="Prénom 4 facultatif" minlength="3" maxlength="10">     
                            <input type="hidden" name="token" id="token" value="'.$token.'"/>                                
                            <input type="submit" value="Valider">
                        </form>';
            }
            else{
                header('Location: /introduction');
            }
            include("view/introduction_config.phtml");
        }
        else{
            header('Location: /introduction');
        }
    }


