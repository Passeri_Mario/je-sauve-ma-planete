<?php

    $mode = "prod"; // debug ou prod

    // Info du jeu générale
    $nom_ecole = "Collège Sophie Germain";
    $titre_du_jeu = "Mission <br> Je sauve ma planète Terre";
    $description_du_jeu = "Outil pédagogique sur les écosystèmes <br> et le recyclage pour élèves de sixième";
    $version_du_jeu = "1.0.0";
    $image_licence = "assets/img/splash/dwca.png";


